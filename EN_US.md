<div style="display: flex; justify-content: center; align-items: center;">
  <img src="./images/perfil.png" alt="Perfil" height="100" width="100" />
  <h1 style="margin-left: 10px;">Hello 👋, I'm Jéferson Barbosa Fischer</h1>
</div>
<br>
<p text-align="center">
With over ten years of experience in software development, I began my career at SEW Eurodrive Brasil Ltda as an Electronic Field Technician, where I developed and integrated automation software for machine control, working with C/C++, Java, and other languages. I then worked as an Automation Analyst at Torfresma Industrial, developing and implementing software in languages ​​aimed at programmable logic controllers (PLCs) and human-machine interfaces (HMIs).

</p>
<p text-align="center">
I am currently an IT Specialist at Lojas Renner S.A., focused on developing APIs and microservices, code review, troubleshooting, and designing technical solutions. My experience in Java, Spring Boot, and messaging systems (Kafka, Rabbit) has been essential for me to face complex challenges and collaborate with multidisciplinary teams. </p>
<p text-align="center">
At the same time, I constantly seek to improve my skills, always valuing efficient and quality work, cooperating with everyone and helping in the best way possible, with great satisfaction in learning and teaching.
</p>
<h3>
Main skills:
</h3>
Java | Spring Boot | Microservices | Kafka | MySQL | RESTful APIs | TDD | Agile Methodologies
<br>
<br>
- 📚 Currently, I'm studying more about **Node.js**
- 💻 You can find my projects on [GitLab](https://gitlab.com/dashboard/projects) or [GitHub](https://github.com/JefersonFischer)
- 🔥 Main stacks: **Spring Framework, Spring Boot, Java, React**
- 📫 How to reach me: **jeferson_fischer@outlook.com**

<h3 align="left">Connect with me:</h3>
<p align="left">
  <a href="https://www.linkedin.com/in/j%C3%A9ferson-fischer-2b97a2124/" target="blank"><img align="center" src="./images/linked-in-alt.svg" height="30" width="40" /></a>
</p>

<h2 align="center">Technologies and Tools Used</h2>
<h3>Languages</h3>
<p align="left">
  <a href="https://www.java.com/" target="_blank" rel="noreferrer"> <img src="./images/java.svg" alt="java" width="40" height="40"/> </a>
  <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank" rel="noreferrer"> <img src="./images/javascript.svg" alt="javascript" width="40" height="40"/> </a>
  <a href="https://www.typescriptlang.org/" target="_blank" rel="noreferrer"> <img src="./images/typescript.svg" alt="typescript" width="40" height="40"/> </a>
</p>

<h3>Frameworks</h3>
<p align="left">
  <a href="https://spring.io/" target="_blank" rel="noreferrer"> <img src="./images/spring.svg" alt="spring" width="40" height="40"/> </a>
  <a href="https://react.dev/blog/2023/03/16/introducing-react-dev" target="_blank" rel="noreferrer"> <img src="./images/react.svg" alt="react" width="40" height="40"/> </a>
  <a href="https://nodejs.org/" target="_blank" rel="noreferrer"> <img src="./images/nodejs.svg" alt="node" width="40" height="40"/> </a>
</p>

<h3>Technologies and Specifications</h3>
<p align="left">
  <a href="https://developer.mozilla.org/en-US/docs/Web/CSS" target="_blank" rel="noreferrer"> <img src="./images/css.svg" alt="css" width="40" height="40"/> </a>
  <a href="https://graphql.org/" target="_blank" rel="noreferrer"> <img src="./images/graphql.svg" alt="graphql" width="40" height="40"/> </a>
  <a href="https://www.w3.org/html/" target="_blank" rel="noreferrer"> <img src="./images/html5.svg" alt="html5" width="40" height="40"/> </a>
</p>

<h3>Tools</h3>
<p align="left">
  <a href="https://www.docker.com/" target="_blank" rel="noreferrer"> <img src="./images/docker.svg" alt="docker" width="40" height="40"/> </a>
  <a href="https://git-scm.com/" target="_blank" rel="noreferrer"> <img src="./images/git-scm.svg" alt="git-scm" width="40" height="40"/> </a>
  <a href="https://kafka.apache.org/" target="_blank" rel="noreferrer"> <img src="./images/kafka.svg" alt="kafka" width="40" height="40"/> </a>
  <a href="https://kubernetes.io/" target="_blank" rel="noreferrer"> <img src="./images/kubernetes.svg" alt="kubernetes" width="40" height="40"/> </a>
  <a href="https://postman.com/" target="_blank" rel="noreferrer"> <img src="./images/postman.svg" alt="postman" width="40" height="40"/> </a>
</p>

<h3>Databases</h3>
<p align="left">
  <a href="https://www.mongodb.com/" target="_blank" rel="noreferrer"> <img src="./images/mongodb.svg" alt="mongodb" width="40" height="40"/> </a>
  <a href="https://www.mysql.com/" target="_blank" rel="noreferrer"> <img src="./images/mysql.svg" alt="mysql" width="40" height="40"/> </a>
  <a href="https://www.postgresql.org/" target="_blank" rel="noreferrer"> <img src="./images/postgresql.svg" alt="postgresql" width="40" height="40"/> </a>
  <a href="https://redis.io/" target="_blank" rel="noreferrer"> <img src="./images/redis.svg" alt="redis" width="40" height="40"/> </a>
  <a href="https://www.sqlite.org/" target="_blank" rel="noreferrer"> <img src="./images/sqlite.svg" alt="sqlite" width="40" height="40"/> </a>
</p>
