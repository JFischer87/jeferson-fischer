<div style="display: flex; justify-content: left; align-items: center;">
  <img src="./images/perfil.png" alt="Perfil" height="100" width="100" />
  <h1 style="margin-left: 10px;">Hello 👋, I'm Jéferson Barbosa Fischer</h1>
</div>
<br>
<p text-align="center">
Com mais de dez anos de experiência em desenvolvimento de software, iniciei minha trajetória na SEW Eurodrive Brasil Ltda como Técnico Eletrônico de Campo, onde desenvolvi e integrei softwares de automação para controle para máquinas, onde atuei com C/C++, Java, entre outras linguagens. Em seguida, atuei como Analista de Automação na Torfresma Industrial, desenvolvendo e implementando software em liguagens voltadas para controladores lógicos programáveis (CLPs) e interfaces homem-máquina (IHMs). 
</p>
<p text-align="center">
Atualmente, sou Especialista em TI na Lojas Renner S.A., focado no desenvolvimento de APIs e microserviços, revisão de código, resolução de problemas e design de soluções técnicas. Minha experiência em Java, Spring Boot e sistema com mensagerias (Kafka, Rabbit) tem sido essencial para enfrentar desafios complexos e colaborar com equipes multidisciplinares.
</p>
<p text-align="center">
Paralelamente, busco constantemente aprimorar minhas competências, prezo sempre pelo trabalho eficiente e de qualidade, cooperando com todos e ajudando da melhor maneira possível, com grande satisfação em aprender e ensinar.
</p>
<h3>
Principais competências:
</h3>
Java | Spring Boot | Microservices | Kafka | MySQL | RESTful APIs | TDD | Agile Methodologies
<br>
<br>

- 📚 Atualmente estou estudando mais sobre **Node.js**
- 💻 Meus projetos podem ser encontrados em [gitlab](https://gitlab.com/dashboard/projects) ou ainda [github](https://github.com/JefersonFischer)
- 🔥 Principais stacks **Spring Framework, Spring Boot, Java, React**
- 📫 Como entrar em contato **jeferson_fischer@outlook.com**

<h3 align="left">Connect with me:</h3>
<p align="left">
<a href="https://www.linkedin.com/in/j%C3%A9ferson-fischer-2b97a2124/" target="blank"><img align="center" src="./images/linked-in-alt.svg" height="30" width="40" /></a>

<h2 align="center">Tecnologias e Ferramentas Utilizadas</h2>
<h3>Linguagens</h3>
<p align="left">
  <a href="https://www.java.com/" target="_blank" rel="noreferrer"> <img src="./images/java.svg" alt="java" width="40" height="40"/> </a>
  <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank" rel="noreferrer"> <img src="./images/javascript.svg" alt="javascript" width="40" height="40"/> </a>
  <a href="https://www.typescriptlang.org/" target="_blank" rel="noreferrer"> <img src="./images/typescript.svg" alt="typescript" width="40" height="40"/> </a>
</p>

<h3>Frameworks</h3>
<p align="left">
  <a href="https://spring.io/" target="_blank" rel="noreferrer"> <img src="./images/spring.svg" alt="spring" width="40" height="40"/> </a>
  <a href="https://react.dev/blog/2023/03/16/introducing-react-dev" target="_blank" rel="noreferrer"> <img src="./images/react.svg" alt="react" width="40" height="40"/> </a>
  <a href="https://nodejs.org/" target="_blank" rel="noreferrer"> <img src="./images/nodejs.svg" alt="node" width="40" height="40"/> </a>
</p>

<h3>Tecnologias e Especificações</h3>
<p align="left">
  <a href="https://developer.mozilla.org/en-US/docs/Web/CSS" target="_blank" rel="noreferrer"> <img src="./images/css.svg" alt="css" width="40" height="40"/> </a>
  <a href="https://graphql.org/" target="_blank" rel="noreferrer"> <img src="./images/graphql.svg" alt="graphql" width="40" height="40"/> </a>
  <a href="https://www.w3.org/html/" target="_blank" rel="noreferrer"> <img src="./images/html5.svg" alt="html5" width="40" height="40"/> </a>
</p>

<h3>Ferramentas</h3>
  <a href="https://www.docker.com/" target="_blank" rel="noreferrer"> <img src="./images/docker.svg" alt="docker" width="40" height="40"/> </a>
  <a href="https://git-scm.com/" target="_blank" rel="noreferrer"> <img src="./images/git-scm.svg" alt="git-scm" width="40" height="40"/> </a>
  <a href="https://kafka.apache.org/" target="_blank" rel="noreferrer"> <img src="./images/kafka.svg" alt="kafka" width="40" height="40"/> </a>
  <a href="https://kubernetes.io/" target="_blank" rel="noreferrer"> <img src="./images/kubernetes.svg" alt="kubernetes" width="40" height="40"/> </a>
  <a href="https://postman.com/" target="_blank" rel="noreferrer"> <img src="./images/postman.svg" alt="postman" width="40" height="40"/> </a>
  
<h3>Banco de dados</h3>
<p align="left">
  <a href="https://www.mongodb.com/" target="_blank" rel="noreferrer"> <img src="./images/mongodb.svg" alt="mongodb" width="40" height="40"/> </a>
  <a href="https://www.mysql.com/" target="_blank" rel="noreferrer"> <img src="./images/mysql.svg" alt="mysql" width="40" height="40"/> </a>
  <a href="https://www.postgresql.org/" target="_blank" rel="noreferrer"> <img src="./images/postgresql.svg" alt="postgresql" width="40" height="40"/> </a>
  <a href="https://redis.io/" target="_blank" rel="noreferrer"> <img src="./images/redis.svg" alt="redis" width="40" height="40"/> </a>
  <a href="https://www.sqlite.org/" target="_blank" rel="noreferrer"> <img src="./images/sqlite.svg" alt="sqlite" width="40" height="40"/> </a>
</p>
